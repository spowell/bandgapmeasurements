import matplotlib.pyplot as plt
import usbtmc
import time
import pyvisa as visa
import numpy as np
from struct import unpack
from matplotlib import pylab
from datetime import datetime
from datetime import date
import re

now = datetime.now()
current_time = now.strftime("%H_%M_%S")

today = date.today()
d1 = today.strftime("%d-%m-%Y-")
basefilename = "./dataFiles/" + d1 + current_time# + ".csv"

def printCSVfile(setvoltages,measuredvoltages):


	# now = datetime.now()
	# current_time = now.strftime("%H_%M_%S")
	
	# today = date.today()
	# d1 = today.strftime("%d-%m-%Y-")
	filename = basefilename+".csv"
	print("file saved... ",filename)
	f = open(filename, "a")

	line = "setvoltage,measuredvoltage\n"  
	f.write(line)
	for x in range(0,len(setvoltages)):
		print(x)
		line = str(setvoltages[x])+","+str(measuredvoltages[x])+"\n"  
		f.write(line)

	f.close()
	pass

def setVoltageOnSourceKiethley(scope, voltage):
 
	scope.write(':SENS:CURR:PROT 3e-3')
	scope.write(':SOUR:VOLT:LEV %f' % voltage)
	scope.write(':OUTP ON')
	
	stringData = 0.0 
	
	return stringData

def readVoltageFromMeasurementKiethley(scope2):

	scope2.write(':SOUR:FUNC CURR')
	scope2.write(':SENS:FUNC "VOLT"')
	scope.write(':SENS:VOLT:PROT 3.0')
	scope2.write(':OUTP ON')
	print("function = ", scope2.query(':SENS:function?'))
	returnValues = scope2.query('READ?')
	print("voltage = ", returnValues)

	voltage = returnValues
	return voltage

rm = visa.ResourceManager()
print(rm.list_resources())
scope = rm.open_resource('GPIB0::22::INSTR')
scope2 = rm.open_resource('ASRL3::INSTR')

scope.encoding =  "latin-1"
scope2.encoding =  "latin-1"
# scope.timeout =  12
print(scope.query('*IDN?'))

settlingTime = 1

setvoltages = []
measuredvoltages = []

# measuredvoltages = [1,2,3]
# setvoltages = [20,40,60]

for setVoltage in range(0, 181, 1):
# for setVoltage in range(0, 19, 1):
	setVoltageOnSourceKiethley(scope, setVoltage/100)
	time.sleep(settlingTime)
	voltageReadings = readVoltageFromMeasurementKiethley(scope2)

	match_number = re.compile('-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *-?\ *[0-9]+)?')
	final_list = [float(x) for x in re.findall(match_number, voltageReadings)]
	voltage=final_list[0]
	print("measured voltage = ", voltage)
	setvoltages.append(setVoltage/100)
	measuredvoltages.append(voltage)

printCSVfile(setvoltages,measuredvoltages)
# x axis values
# x = [1,2,3]
# corresponding y axis values
# y = [2,4,1]
# plotting the points
plt.plot(setvoltages, measuredvoltages)
# naming the x axis
plt.xlabel('Set voltage (V)')
# naming the y axis
plt.ylabel('Measured voltage (V)')
# giving a title to my graph
plt.title('Input Voltage vs Output Voltage BGR')
# function to show the plot
filename = basefilename+".png"
plt.savefig(filename)

plt.show()
