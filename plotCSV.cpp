#include <sstream>
#include <string>
#include <fstream>
#include <cstddef>
#include <algorithm>

{


	TCanvas *c1 = new TCanvas();

   const int n = 2350;
   double x[n], y[n];

	std::string line;
	std::ifstream infile("mosfetBGRtemp1.csv");

	int i =0;
	
	cout << "hello \n\n";

	while (std::getline(infile, line))
	{
		cout<< i << "\n";
		if (i < n)
		{
			/* code */
		
		    std::istringstream iss(line);

			string data1_str, data2_str, data3_str, data4_str;
			double Data1, Data2, Data3, Data4;
			
			cout << "\n line = " << line << endl;

			std::stringstream ss(line);
			// cout <<"line snip = "<< line << endl;
			getline(ss, data1_str, ',');
			getline(ss, data2_str, ',');      
			getline(ss, data3_str, ',');      
			getline(ss, data4_str, ',');      

			// cout << data1_str <<"\n";
			// cout << data2_str <<"\n";
			// cout << data3_str <<"\n";
			// cout << data4_str <<"\n";

			if (i>0)
			{
				// Data1 = stod(data1_str);
				// Data2 = stod(data2_str);
				// Data3 = stod(data3_str);
				// Data4 = stod(data4_str);

				std::istringstream os(data1_str);
				os >> Data1;
				std::istringstream os2(data2_str);
				os2 >> Data2;

				cout << Data1 <<"\n";
				cout << Data2 <<"\n";
				// cout << Data3 <<"\n";
				// cout << Data4 <<"\n";

				x[i-1] = Data1;
				y[i-1] = Data2;
			}
		}	
			
		i++;
	}

	// cout << "x values = " << x << "\n\n";
	// cout << "x values = " << x << "\n\n";
	TGraph *tg  = new TGraph(n,x,y);

	// tg->SetTitle("BGR Output voltage vs Input voltage");
	// tg->GetXaxis()->SetTitle("Input voltage");
	// tg->GetYaxis()->SetTitle("Output voltage");
	// // tg->Draw("*AC");

	TMultiGraph *mg = new TMultiGraph();
	mg->Add(tg);

	i =0;

	std::string line2;
	std::ifstream infile2("mosfetBGRtemp2.csv");
	
	while (std::getline(infile2, line2))
	{
		if (i < n)
		{
			/* code */
		
		   std::istringstream iss(line2);

			string data1_str, data2_str, data3_str, data4_str;
			double Data1, Data2, Data3, Data4;
			
			cout << "\n file 2 line = " << line2 << endl;

			std::stringstream ss(line2);
			// cout <<"line snip = "<< line << endl;
			getline(ss, data1_str, ',');
			getline(ss, data2_str, ',');      
			getline(ss, data3_str, ',');      
			getline(ss, data4_str, ',');      

			// cout << data1_str <<"\n";
			// cout << data2_str <<"\n";
			// cout << data3_str <<"\n";
			// cout << data4_str <<"\n";

			if (i>0)
			{
				Data1 = stod(data1_str);
				Data2 = stod(data2_str);
				Data3 = stod(data3_str);
				Data4 = stod(data4_str);

				// std::istringstream os3(data1_str);
				// os3 >> Data1;
				// std::istringstream os4(data2_str);
				// os4 >> Data2;

				cout << Data1 <<"\n";
				cout << Data2 <<"\n";
				cout << Data3 <<"\n";
				cout << Data4 <<"\n";

				x[i-1] = Data1;
				y[i-1] = Data3;
			}
		}	
			
		i++;
	}

	// cout << "x values = " << x << "\n\n";
	// cout << "x values = " << x << "\n\n";

	TGraph *tg2  = new TGraph(n,x,y);

	// tg->SetTitle("BGR Output voltage vs Input voltage");
	// tg->GetXaxis()->SetTitle("Input voltage");
	// tg->GetYaxis()->SetTitle("Output voltage");
	// tg->Draw("*AC");
	
	// mg->SetTitle("BGR Output voltage vs Input voltage");
	// mg->GetXaxis()->SetTitle("Input voltage (V)");
	// mg->GetYaxis()->SetTitle("Output voltage (V)");
	
	mg->SetTitle("BGR Output voltage vs Temperature");
	mg->GetXaxis()->SetTitle("Temperature (*C)");
	mg->GetYaxis()->SetTitle("Output voltage (V)");
	
	mg->Add(tg2);
	mg->Draw("*AC");

	mg->GetYaxis()->SetLimits(0.5,1);
	mg->SetMinimum(0.34);
   // mg->SetMaximum(10.);
	
	tg->SetLineColor(kRed);
	tg2->SetLineColor(kBlue);

	TLegend *leg = new TLegend(0, 10, 0, 10);
	leg->SetFillColor(0);
	// leg->SetHeader("legend");
	leg->AddEntry(tg, "simulated", "lp");
	leg->AddEntry(tg2, "measured", "lp");
	// leg->AddEntry(gr3, "graph 3", "lp");
	leg->Draw();

}