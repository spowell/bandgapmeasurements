import matplotlib.pyplot as plt
import usbtmc
import time
import pyvisa as visa
import numpy as np
from struct import unpack
from matplotlib import pylab
from datetime import datetime
from datetime import date
import re

now = datetime.now()
current_time = now.strftime("%H_%M_%S")

today = date.today()
d1 = today.strftime("%d-%m-%Y-")
basefilename = "./dataFiles/" + d1 + current_time# + ".csv"

def printCSVfile(setvoltages,measuredvoltages):


	# now = datetime.now()
	# current_time = now.strftime("%H_%M_%S")
	
	# today = date.today()
	# d1 = today.strftime("%d-%m-%Y-")
	filename = basefilename+".csv"
	print("file saved... ",filename)
	f = open(filename, "a")

	line = "setvoltage,measuredvoltage\n"  
	f.write(line)
	for x in range(0,len(setvoltages)):
		print(x)
		line = str(setvoltages[x])+","+str(measuredvoltages[x])+"\n"  
		f.write(line)

	f.close()
	pass

def setVoltageOnSourceKiethley(scope, voltage):
 
	scope.write(':SENS:CURR:PROT 3e-3')
	scope.write(':SOUR:VOLT:LEV %f' % voltage)
	scope.write(':OUTP ON')
	
	stringData = 0.0 
	
	return stringData

def setCurrentOnSourceKiethley(scope, current):
 
    scope.write(':SOUR:FUNC CURR')
    # scope.write(':SENS:CURR:PROT 3e-3')
    scope.write(':SOUR:CURR:RANG 300e-3')
    # scope.write(':SENS:VOLT:PROT 13.0')
    scope.write(':SOUR:CURR:LEV %f' % current)
    scope.write(':OUTP ON')

    stringData = 0.0 
	
    return stringData

# def readVoltageFromCurrentSourceKiethley(scope2):

# 	scope.write(':SOUR:FUNC CURR')
# 	scope.write(':SENS:FUNC "VOLT"')
# 	# scope.write(':SENS:VOLT:PROT 3.0')
# 	scope.write(':OUTP ON')
# 	print("function = ", scope.query(':SENS:function?'))
# 	returnValues = scope.query('READ?')
# 	print("voltage = ", returnValues)

# 	voltage = returnValues
# 	return voltage

def readVoltageFromMeasurementKiethley(scope2):

	scope2.write(':SOUR:FUNC CURR')
	scope2.write(':SENS:FUNC "VOLT"')
	scope.write(':SENS:VOLT:PROT 3.0')
	scope2.write(':OUTP ON')
	print("function = ", scope2.query(':SENS:function?'))
	returnValues = scope2.query('READ?')
	print("voltage = ", returnValues)

	voltage = returnValues
	return voltage

rm = visa.ResourceManager()
print(rm.list_resources())

#########################################################



scope = rm.open_resource('ASRL/dev/ttyUSB0::INSTR', write_termination='\r',
                           read_termination='\r', baud_rate=9600)
scope2 = rm.open_resource('ASRL/dev/ttyUSB1::INSTR', write_termination='\r',
                           read_termination='\r', baud_rate=9600)


print("scope 1 id = ")
print(scope.query('*IDN?'))

print("scope 2 id = ")
print(scope2.query('*IDN?'))


settlingTime = 5

setCurrents = []
measuredvoltages = []

currentSteps = -1e-3

# for setVoltage in range(0, 181, 1):
for setCurrent in range(0, 85, 1):

    setCurrentOnSourceKiethley(scope, setCurrent*currentSteps)
    time.sleep(settlingTime)
    voltageReadings = readVoltageFromMeasurementKiethley(scope2)

    match_number = re.compile('-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *-?\ *[0-9]+)?')
    final_list = [float(x) for x in re.findall(match_number, voltageReadings)]
    voltage=final_list[0]

    print("set Current = ", setCurrent*currentSteps)
    print("measured voltage = ", voltage)

    setCurrents.append(setCurrent*currentSteps)
    measuredvoltages.append(voltage)

printCSVfile(setCurrents,measuredvoltages)
# plotting the points
plt.plot(setCurrents, measuredvoltages)
# naming the x axis
plt.xlabel('Load current (A)')
# naming the y axis
plt.ylabel('Measured output voltage (V)')
# giving a title to my graph
plt.title('Shunt regulator Output Voltage vs load current (Rshunt = 12 Ohms)')
# function to show the plot
filename = basefilename+".png"
plt.savefig(filename)
plt.show()
