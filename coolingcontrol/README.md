# Control and readout for the Liverpool cooling system used for the characterisation of HV-CMOS chips

## System overview

Cooling system controlled via two Arduinos:

**Arduino Nano** (in black control box; port /ttyUSB0, baud rate 9600) controls:
- 3 x 100k NTC thermistors in/around copper block (fourth one not connected)
- 2 x 120mm fans on the heatsink (run at const. 12 V)
- 1 x Peltier element (theoretically up to 16 V), controlled via two MOSFETS
- 1 x cartridge heater (optional), used primarily for simulating heat from chip

**Arduino Uno** (port /ttyACM0, baud rate 57600) controls:
- 1 x HYT221 humidity and temperature sensor


## Directory overview

### ArduinoCode

Contains the code for both Arduinos

- **HumidityReadout**: Code for the *Arduino Uno* controlling the HYT221 humidity and temperature sensor
- **NoPID**: Code for the *Arduino Nano* controlling the Peltier element, fans, and heater, and reading out the thermistors; has no PID control implemented, just full power to peltier for maximum cooling
- **OnlyTempReadout**: Code for the *Arduino Nano*, only readout of thermistors; peltier, fans, and heater off
- **PIDTempControl**: Code for the *Arduino Nano* controlling the Peltier element, fans, and heater via a PID controller, and reading out the thermistors; desired temerature can be system

### Docs

Documents and datasheets for various parts of the cooling system

### Logs

Contains all the log files of the conducted measurements

### Pictures

Contains pictures of the cooling system

### Plots

Contains all the measurement plots made from the log files

### PythonLog

Python code for creating the logs
- **hum_log.py**: logs only the humidity and temperature recorded by the HYT221 sensor
- **temp_hum_log.py**: main logger, records temperature of the three thermistors, the average temperature calculated from the three, and the humdity and temperature from the HYT221 sensor
- **test_temp_log.py**: code for playing around with the logger without screwing up the actual one - especially for debugging

### PythonPlot

Python code for plotting different things from the log files
