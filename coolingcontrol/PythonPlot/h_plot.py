#########################################################
#   Plotter for Cooling System humidity log             #
#   (HYT221 humidity readout)                           #
#                                                       #
#   Author: Sigrid Scherl                               #
#   Date: JUly 2021                                     #
#                                                       #
#   Uses Python 3                                       #
#########################################################

from __future__ import print_function
from math import sin
from array import array
import numpy as np
import matplotlib.pyplot as plt
import sys
import glob

exp = np.exp
sqrt = np.sqrt
ln = np.log

#===== Path to directory =====================================

#directory = '/home/labuser/CoolingControl/Tlogs/'
directory = 'C:/Users/sscherl/Documents/UoL/PhD/Projects/CoolingSystem/'

#===== Get all files in directory to plot everything =====================================

file_list, file_names, file_dates, file_times = [], [], [], []
file_year, file_month, file_day = [], [], []
file_hour, file_minute, file_second = [], [], []

file_list = glob.glob(directory+'*.txt')

for file in file_list:
    file_names.append(file[len(directory):])

for file in file_names:
    file_dates.append(file[5:15])     # numbers valid for lab pc
    file_times.append(file[16:24])    # numbers valid for lab pc
    
for file in file_dates:
    file_year.append(file[:4])
    file_month.append(file[5:7])
    file_day.append(file[8:10])
    
for file in file_times:
    file_hour.append(file[:2])
    file_minute.append(file[3:5])
    file_second.append(file[6:8])

#===== Function to read data and plot it ==================

def plotter(file_path, file_name, setpoint, kp, ki, kd):

    #===== Get file info ==================================
    
    file_date, file_time, file_year, file_month, file_day, file_hour, file_minute, file_second = [], [], [], [], [], [], [], []
    
    file_date = file_name[6:16]     # numbers valid for laptop
    file_time = file_name[17:25]    # numbers valid for laptop

    file_year = file_date[:4]
    file_month = file_date[5:7]
    file_day = file_date[8:10]
    
    file_hour = file_time[:2]
    file_minute = file_time[3:5]
    file_second = file_time[6:8]
    
    #===== Declare variables ==============================

    data = []
    rel_time = []
    avgT, T0, T1, T2 = [], [], [], []
    hyt_t, hyt_h = [], []
    
    #===== Read data ======================================
     
    with open(file_path+file_name,'r') as file:
        for line in file:
             data.append(line)
        file.close()
        
    #=== Remove header lines ===  
    for i in range(4):
     	data.remove(data[0])
    
    line = np.zeros((len(data)))

    for i in range(len(data)):
        line = data[i].split('\t')
        
        #=== Start time of measurement ===
        if i == 0:
            start_time = line[0]
            start_hour = float(start_time[:2])
            start_min  = float(start_time[3:5])
            start_sec  = float(start_time[6:8])

        #=== Absolute time of measurement ===
        abs_time = line[0]
        abs_hour = float(abs_time[:2])
        abs_min  = float(abs_time[3:5])
        abs_sec  = float(abs_time[6:8])
        
        #=== Relative time of measurement - for plot ===
        rel_hour = abs_hour - start_hour
        rel_min  = abs_min  - start_min
        rel_sec  = abs_sec  - start_sec
        
        #=== Relative time of measurement in minutes ===
        rel_time.append((rel_hour * 3600 + rel_min * 60 + rel_sec)/60)
            
        #=== Thermistor data ===
        avgT.append(float(line[1]))
        T0.append(float(line[3]))
        T1.append(float(line[5]))
        T2.append(float(line[7]))
        
        #=== Humidity sensor 'HYT221' data ===
        hyt_h.append(float(line[9]))
        hyt_t.append(float(line[11]))

    #===== Plot data ======================================
    
    #=== Chose how many data points to chop off at start and end (for outliers) ===
    chop_start = 2
    chop_end = len(rel_time) - 0
    
    #=== Y-axis 1: temperature ===
    fig, ax1 = plt.subplots()
    
    ax1.set_xlabel('Time (min)')
    ax1.set_ylabel('Temperature (C)')
    ax1.set_ylim(19.5,21)
    ax1.tick_params(axis = 'y', labelcolor = 'mediumblue')
    
    ax1.plot(rel_time[chop_start:chop_end], T0[chop_start:chop_end],    label = 'T0',       color = 'paleturquoise',linestyle = '-',    linewidth = 0.5)
    ax1.plot(rel_time[chop_start:chop_end], T1[chop_start:chop_end],    label = 'T1',       color = 'lightskyblue', linestyle = '-',    linewidth = 0.5)
    ax1.plot(rel_time[chop_start:chop_end], T2[chop_start:chop_end],    label = 'T2',       color = 'lightblue',    linestyle = '-',    linewidth = 0.5)
    ax1.plot(rel_time[chop_start:chop_end], avgT[chop_start:chop_end],  label = 'Mean T',   color = 'mediumblue',   linestyle = '-',    linewidth = 1)
    ax1.plot(rel_time[chop_start:chop_end], hyt_t[chop_start:chop_end], label = 'HYT T',    color = 'darkblue',     linestyle = '-',    linewidth = 1)
    # ax1.plot([rel_time[0], rel_time[-1]],   [setpoint,setpoint],        label = "Setpoint", color = 'black',        linestyle = '--',   linewidth = 1)
    
    #=== Y-axis 2: humidity ===
    ax2 = ax1.twinx()
    
    ax2.set_ylabel('Relative humidity (%)')
    ax2.set_ylim(0,50)
    # ax2.set_yscale('log')
    ax2.tick_params(axis = 'y', labelcolor = 'crimson')
    
    ax2.plot(rel_time[chop_start:chop_end], hyt_h[chop_start:chop_end], label = 'RH',       color ='crimson',       linestyle = '-',    linewidth = 1)

    #=== Figure ===
    fig.tight_layout()
    fig.legend(bbox_to_anchor=[0.5,0.02], loc='upper center', ncol=6, frameon = True)

    plt.title('Cooling System Temperature Measurement\n Date: '+file_day+'/'+file_month+'/'+file_year+', Start time: '+file_hour+':'+file_minute+':'+file_second+'\nk_p = '+str(kp)+', k_i = '+str(ki)+', k_d = '+str(kd))
    plt.savefig(file_path[:-5]+'Plots/'+file_name[:-4]+'.png', dpi = 200, bbox_inches='tight')
    
#===== Plot data from single file =====================================

# plotter(directory+'Logs/', 'THlog_2021-07-01_16-21-30.txt', 15, 3, 1.5, 5)  #plotter(file_path, file_name, setpoint, kp, ki, kd)
plotter(directory+'Logs/', 'THlog_2021-07-01_16-42-34.txt', 15, 3, 1.5, 5)  #plotter(file_path, file_name, setpoint, kp, ki, kd)

#===== Plot data from all files in folder =====================================

# for file in file_names:
#     plotter(directory, file)
