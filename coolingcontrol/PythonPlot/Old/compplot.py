#########################################################
#	Plotter for Cooling System 			#
#	average temperatures comparison			#
#							#
#	Created by Sigrid in May 2021			#
#							#
#	Uses Python 3					#
#########################################################


from __future__ import print_function
from math import sin
from array import array
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import sys
import glob

exp = np.exp
sqrt = np.sqrt
ln = np.log

#===== Get file names =====================================

directory = '/home/labuser/CoolingControl/Tlogs/'

#===== Function to read data and plot it ==================
def avgTof(file_path, file_name, kp, ki, kd, col):          # filename with directory!
    
    #===== Get file info ==================================
    
    file_date, file_time, file_year, file_month, file_day, file_hour, file_minute, file_second = [], [], [], [], [], [], [], []
    
    file_date = file_name[5:15]
    file_time = file_name[16:24]

    file_year = file_date[:4]
    file_month = file_date[5:7]
    file_day = file_date[8:10]
    
    file_hour = file_time[:2]
    file_minute = file_time[3:5]
    file_second = file_time[6:8]
    
    #===== Declare variables ==============================

    data = []
    data2 = np.zeros((len(data)))

    Time, hours, minutes, seconds = [], [], [], []
    rel_hours, rel_minutes, rel_seconds, rel_time = [], [], [], []
    avgT, T0, T1, T2 = [], [], [], []

    #===== Read data ======================================
    
    
    with open(file_path+file_name,'r') as file:
        for line in file:
             data.append(line)
        file.close()
    
    for i in range(4):
     	data.remove(data[0])
    
    for i in range(len(data)):
        data2 = data[i].split('\t')
        Time.append(data2[0])
        hours.append(float(Time[-1][:2]))
        minutes.append(float(Time[-1][3:5]))
        seconds.append(float(Time[-1][6:8]))
        
        rel_hours.append(hours[i] - hours[0])
        rel_minutes.append(minutes[i] - minutes[0])
        rel_seconds.append(seconds[i] - seconds[0])
        
        rel_time.append((rel_hours[-1] * 3600 + rel_minutes[-1] * 60 + rel_seconds[-1])/60) #divide by 60 to get in minutes
        
        avgT.append(float(data2[1]))
        T0.append(float(data2[3]))
        T1.append(float(data2[5]))
        T2.append(float(data2[7]))

    plot_avgT = plt.plot(rel_time,avgT, label = 'kp = '+kp+', ki = '+ki+', kd = '+kd)
    plt.setp(plot_avgT, color=col, linewidth=1)
              
#===== Plot data ======================================

plot_Setpoint = plt.plot([0,9.2], [15,15], '--', label = "Setpoint")
plt.setp(plot_Setpoint, color='black', linewidth=1)

avgTof(directory, 'Tlog_2021-05-18_12-52-49.txt', kp = '1', ki = '1', kd = '1', col = 'brown')
avgTof(directory, 'Tlog_2021-05-18_13-10-06.txt', kp = '2', ki = '1', kd = '1', col = 'red')
avgTof(directory, 'Tlog_2021-05-18_13-31-18.txt', kp = '3', ki = '1', kd = '1', col = 'orange')
avgTof(directory, 'Tlog_2021-05-18_14-14-36.txt', kp = '1', ki = '2', kd = '1', col = 'gold')
avgTof(directory, 'Tlog_2021-05-18_14-32-40.txt', kp = '1', ki = '3', kd = '1', col = 'yellowgreen')
avgTof(directory, 'Tlog_2021-05-18_14-50-39.txt', kp = '1', ki = '1', kd = '2', col = 'green')
avgTof(directory, 'Tlog_2021-05-18_15-08-19.txt', kp = '1', ki = '1', kd = '3', col = 'turquoise')
avgTof(directory, 'Tlog_2021-05-20_12-08-16.txt', kp = '2', ki = '1', kd = '2', col = 'dodgerblue')
avgTof(directory, 'Tlog_2021-05-20_12-30-15.txt', kp = '3', ki = '1', kd = '2', col = 'blue')
avgTof(directory, 'Tlog_2021-05-20_12-52-13.txt', kp = '2', ki = '1', kd = '3', col = 'darkviolet')
avgTof(directory, 'Tlog_2021-05-20_13-13-45.txt', kp = '3', ki = '1', kd = '3', col = 'magenta')
avgTof(directory, 'Tlog_2021-05-20_13-34-49.txt', kp = '2', ki = '2', kd = '2', col = 'pink')
avgTof(directory, 'Tlog_2021-05-20_13-57-18.txt', kp = '3', ki = '2', kd = '3', col = 'lightgray')
avgTof(directory, 'Tlog_2021-05-20_14-38-49.txt', kp = '3', ki = '1.8', kd = '3', col = 'gray')
avgTof(directory, 'Tlog_2021-05-20_15-01-54.txt', kp = '4', ki = '1.8', kd = '4', col = 'black')


plt.xlabel('Time (min)')
plt.ylabel('Average Temperature (C)')
plt.title('Cooling System Temperature Measurement')

plt.ylim(10,22)

plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1), fontsize=7, ncol=3)

plt.savefig(directory+'Plots/CompAvgT.pdf')

#===== Plot stuff =====================================

# for file in file_names:
#     plotter(directory, file)
