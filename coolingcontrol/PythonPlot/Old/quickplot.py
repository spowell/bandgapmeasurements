#########################################################
#	Plotter for Cooling System temperature log	#
#							#
#	Created by Sigrid in May 2021			#
#							#
#	Uses Python 3					#
#########################################################


from __future__ import print_function
from math import sin
from array import array
import numpy as np
import matplotlib.pyplot as plt
import sys
import glob

exp = np.exp
sqrt = np.sqrt
ln = np.log

#===== Get file names =====================================
file_list = []
file_names = []
file_dates = []
file_times = []

file_year, file_month, file_day = [], [], []
file_hour, file_minute, file_second = [], [], []

directory = '/home/labuser/CoolingControl/Tlogs/'

file_list = glob.glob(directory+'*.txt')

for file in file_list:
    file_names.append(file[len(directory):])

for file in file_names:
    file_dates.append(file[5:15])
    file_times.append(file[16:24])
    
for file in file_dates:
    file_year.append(file[:4])
    file_month.append(file[5:7])
    file_day.append(file[8:10])
    
for file in file_times:
    file_hour.append(file[:2])
    file_minute.append(file[3:5])
    file_second.append(file[6:8])


#===== Function to read data and plot it ==================

def plotter(file_path, file_name, setpoint, kp, ki, kd):          # filename with directory!
    
    #===== Get file info ==================================
    
    file_date, file_time, file_year, file_month, file_day, file_hour, file_minute, file_second = [], [], [], [], [], [], [], []
    
    file_date = file_name[5:15]
    file_time = file_name[16:24]

    file_year = file_date[:4]
    file_month = file_date[5:7]
    file_day = file_date[8:10]
    
    file_hour = file_time[:2]
    file_minute = file_time[3:5]
    file_second = file_time[6:8]
    
    #===== Declare variables ==============================

    data = []
    data2 = np.zeros((len(data)))

    Time, hours, minutes, seconds = [], [], [], []
    rel_hours, rel_minutes, rel_seconds, rel_time = [], [], [], []
    avgT, T0, T1, T2 = [], [], [], []


    #===== Read data ======================================
    
    
    with open(file_path+file_name,'r') as file:
        for line in file:
             data.append(line)
        file.close()
    
    for i in range(4):
     	data.remove(data[0])
    
    for i in range(len(data)):
        data2 = data[i].split('\t')
        Time.append(data2[0])
        hours.append(float(Time[-1][:2]))
        minutes.append(float(Time[-1][3:5]))
        seconds.append(float(Time[-1][6:8]))
        
        rel_hours.append(hours[i] - hours[0])
        rel_minutes.append(minutes[i] - minutes[0])
        rel_seconds.append(seconds[i] - seconds[0])
        
        rel_time.append((rel_hours[-1] * 3600 + rel_minutes[-1] * 60 + rel_seconds[-1])/60) #divide by 60 to get in minutes
        
        avgT.append(float(data2[1]))
        T0.append(float(data2[3]))
        T1.append(float(data2[5]))
        T2.append(float(data2[7]))
        
        
    #===== Plot data ======================================
    
    plot_T0 = plt.plot(rel_time,T0, '.', label = 'T0')
    plot_T1 = plt.plot(rel_time,T1, '.', label = 'T1')
    plot_T2 = plt.plot(rel_time,T2, '.', label = 'T2')
    plot_avgT = plt.plot(rel_time,avgT, label = 'Mean T')
    plot_Setpoint = plt.plot([rel_time[0], rel_time[-1]], [setpoint,setpoint], '--', label = "Setpoint")
    
    plt.xlabel('Time (min)')
    plt.ylabel('Temperature (C)')
    plt.title('Cooling System Temperature Measurement\n Date: '+file_day+'/'+file_month+'/'+file_year+', Start time: '+file_hour+':'+file_minute+':'+file_second+'\nk_p = '+str(kp)+', k_i = '+str(ki)+' (abs(error)<3), k_d = '+str(kd)+', 12 V')
    
    #plt.ylim([7,20])

    plt.setp(plot_avgT, color='r', linewidth=1)
    plt.setp(plot_T0, color='b', markersize=2)
    plt.setp(plot_T1, color='g', markersize=2)
    plt.setp(plot_T2, color='y', markersize=2)
    plt.setp(plot_Setpoint, color='black', linewidth=1)

    plt.legend()
    plt.savefig(file_path+'Plots/'+file_name[:-4]+'.pdf')
#    plt.show()
    
#===== Plot stuff =====================================
plotter(directory, 'Tlog_2021-06-04_10-10-32.txt', 15, 3, 1.5, 5)  #plotter(file_path, file_name, setpoint, kp, ki, kd)

# for file in file_names:
#     plotter(directory, file)
