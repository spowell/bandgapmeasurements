// Code and library from https://forum.arduino.cc/t/hyt-221-from-hygrosens-instruments/54560/5

/*
 Programm to measure and transmit Humidity and Temperature 
 HYT-221 via Serial Communication.
 
 Not optimized for high speed measurement. A minimum delay 
 of 100 ms is currently hard coded in the getData() function
 */

#include <Wire.h>
#include <HYT221.h>

HYT221 HTsens01 = HYT221(0x28);

void setup() {
  
   Serial.begin(57600);
   while (!Serial) {
    ; // wait for serial port to connect
   }
   
   // initialize I2C protocoll
   Wire.begin();

   // initialize the sensor
   HTsens01.begin();
}

void loop() {

   HTsens01.read();
   double h = HTsens01.getHumidity();         // humidity in %
   double t = HTsens01.getTemperature();      // temperature in C
   int hraw =  HTsens01.getRawHumidity() ;    // raw value as given by sensor
   int traw =  HTsens01.getRawTemperature() ; // raw value as given by sensor
   
   Serial.print(t);
   Serial.print("|");
   Serial.println(h);
   
   delay(900); //with internal delay of 100 ms this leads to 1 measurement/s
}
