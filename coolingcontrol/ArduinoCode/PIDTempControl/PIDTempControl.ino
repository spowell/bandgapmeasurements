/////////////////////////////////////////////
//        Cooling System Controller        //
//                                         //
//        Temperature readout and          //
//        PID controller                   //
//                                         //
//        Created May 2021                 //
/////////////////////////////////////////////



// Thermistors connected to pin0 to pin3
int pin0 = A0;
int pin1 = A1;
int pin2 = A2;
int pin3 = A3; //not connected to a thermistor (just extra cable)!

// Pins for fan, peltier, and heater
int fanpin = 8;       //120 mm fans on heatsink - high = disable
int peltier = 9;      //constrollable using analogWrite function (PWM pin)
int peltier2 = 10;    //hast to always be the same as pin 9! both mosfets for same peltier (PWM pin)
int heater = 11;    //(optional) controllable using analogWrite function

// PID variables
float Setpoint = 21.0;  // Desired temperature
float Input = 0;         // Actual temperature - given by AVG
int Output = 0;           // comes from PID value goes to both peltier pins
int Output2 = 0;          // heater
float pid_value = 0;        // PID value - goes to output
float pid_error = 0;
float prev_error = 0;
float elapsedTime, Time, timePrev;
float pid_max = 5;
float pid_min = -5;


int interval = 1000; // miliseconds

// PID constants
float kp = 1/*3*/;   float ki = 1.5;     float kd = 5;
float pid_p = 0;  float pid_i = 0;  float pid_d = 0;

// Variables for for temperature readout
int V0;
int V1;
int V2;
float AVG;

int time;

float R1 = 100000;
float logR2, R2, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13;
float c1 =0.7203283552e-3, c2= 2.171656865e-4, c3 = 0.8706070062e-7;

void setup() {
  
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Time = millis();
  
  pinMode (6, OUTPUT);
  pinMode(8, OUTPUT);       //fan
  pinMode(9, OUTPUT);       //peltier1
  pinMode(10, OUTPUT);      //peltier2
  pinMode(11, OUTPUT);  //heater

  // Initialize the variables we're linked to for PID
//  Input = analogRead(AVG);
//  Setpoint = analogRead();  // Could add pin here from which Setpoint (desired value) is read, now const.
//  Setpoint = map(Setpoint, 0, 1023, 0, 255); //needed if converting from analogue pin (0 to 1023) to PWM pin (0 to 255)

}

void loop() {
  
  digitalWrite(fanpin, LOW);    //fan on
//  analogWrite (peltier, 0);   //peltier off - 255 for on
//  analogWrite (peltier2, 0);  //peltier off - 255 for on
//  analogWrite (heater, 200);  //heater on - not used for now

// Temperature calculations

  // Thermistor 0
  V0 = analogRead(pin0);
  R2 = R1 * (1023.0 / (float)V0 - 1.0);
  logR2 = log(R2);
  T0 = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  T0 = T0 - 273.15;
  
  delay(interval/4);

  // Thermistor 1
  V1 = analogRead(pin1);
  R2 = R1 * (1023.0 / (float)V1 - 1.0);
  logR2 = log(R2);
  T1 = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  T1 = T1 - 273.15;

  delay(interval/4);

  // Thermistor 2
  V2 = analogRead(pin2);
  R2 = R1 * (1023.0 / (float)V2 - 1.0);
  logR2 = log(R2);
  T2 = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  T2 = T2 - 273.15;

  delay(interval/4);

  AVG = (T0+T1+T2)/3;

// PID Calculations

  Input = AVG;
  pid_error = Input - Setpoint;

//  if (pid_error > -1){     //Don't use heater - when value below setpoint just wait to warm up again, don't cool further

  // Proportional
  pid_p = kp * pid_error;

//  // Integral
//  if(abs(pid_error) < 3){
//    pid_i = (pid_i + ki * pid_error);
//  }
//  else pid_i = 0;

  // Derivative
  timePrev = Time;
  Time = millis();
  elapsedTime = (Time - timePrev) /1000;

  pid_d = kd * ((pid_error - prev_error) / elapsedTime);
  prev_error = pid_error;

  // Total PID
  pid_value = (pid_p + pid_i + pid_d);

  if(pid_value > pid_max) pid_value = pid_max;
  if(pid_value < pid_min) pid_value = pid_min; 


  if((pid_error < 0) && (Output == 0))
  {
    pid_value = (float)Output2 - pid_value;
    Output = 0;
    
    // PID to Output
    if (pid_value < 0){
      Output2 = 0;
    }
    else if (pid_value > 255){
      Output2 = 255;
    }
    else {
      Output2 = (int)(round(pid_value)) ;
    }
  }
  else
  {
    pid_value = (float)Output + pid_value;
    Output2 = 0;
    
    // PID to Output
    if (pid_value < 0){
      Output = 0;
    }
    else if (pid_value > 255){
      Output = 255;
    }
    else {
      Output = (int)(round(pid_value)) ;
    }
  }
  
  analogWrite(peltier,Output);
  analogWrite(peltier2,Output);
  analogWrite(heater,Output2);

  // Send values to PC log
  //Serial.print(millis()/1000);
  
  Serial.print("incoming data = ");
  String incomingData = Serial.readString();
  Serial.print(incomingData);
  
  int number = incomingData.toInt();
  Serial.print("| number = ");
  Serial.print(number);
  
  if(number != 0)
  {
    Setpoint = number;
  }
  
  Serial.print("| Setpoint = ");
  Serial.print(Setpoint);
  Serial.print("|");
  Serial.print(T2);
  Serial.print("|");
  Serial.print(T1);
  Serial.print("|");
  Serial.println(T0);
  
  
  //Serial.print(T0);
//  Serial.print("|");
//  Serial.print(AVG);
//  Serial.print("|"); //TESTING
//  Serial.print(Output); //TESTING
//  Serial.print("|"); //TESTING
//  Serial.print(Output2); //TESTING
//  Serial.print("|"); //TESTING
//  Serial.print(pid_value); //TESTING
//  Serial.print("|"); //TESTING
//  Serial.print(pid_p + pid_i + pid_d); //TESTING
//  Serial.print("|"); //TESTING
//  Serial.print(pid_p); //TESTING
//  Serial.print("|"); //TESTING
//  Serial.print(pid_i); //TESTING
//  Serial.print("|"); //TESTING
//  Serial.print(pid_d); //TESTING
//  Serial.print("|"); //TESTING
//  Serial.print(pid_error); //TESTING
//  Serial.println("|");
  
  delay(interval/4);
}
