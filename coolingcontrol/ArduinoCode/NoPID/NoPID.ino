/////////////////////////////////////////////
//        Cooling System Controller        //
//                                         //
//        Temperature readout only!        //
//                                         //
//        Created May 2021                 //
/////////////////////////////////////////////

//thermistors connected to pin0 to pin3 (one of them not connected)
int pin0 = A0;
int pin1 = A1;
int pin2 = A2;
int pin3 = A3;

//pins for fan, peltier, and heater
int fanpin = 8;     //120 mm fans on heatsink - high = disable
int peltier = 9;    //controllable using analogWrite function
int peltier2 = 10;  //has to always be the same as pin 9! both mosfets for same peltier
int heater = 11;    //(optional) controllable using analogWrite function

//variables for for temperature readout
int V0;
int V1;
int V2;
int V3;
float AVG;

int time;

float R1 = 100000;
float logR2, R2, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13;
float c1 =0.7203283552e-3, c2= 2.171656865e-4, c3 = 0.8706070062e-7;

void setup() {
  Serial.begin(9600);
  pinMode (6, OUTPUT);
  pinMode(8, OUTPUT);   //fan
  pinMode(9, OUTPUT);   //peltier1
  pinMode(10, OUTPUT);  //peltier2
  pinMode(11, OUTPUT);  //heater
}

void loop() {
  
  digitalWrite(fanpin, LOW);    //LOW = fan on
  analogWrite (peltier, 100);   //peltier on
  analogWrite (peltier2, 100);  //peltier on
  analogWrite (heater, 0);  //heater (46 equals about 1 W)

  V0 = analogRead(pin0);
  R2 = R1 * (1023.0 / (float)V0 - 1.0);
  logR2 = log(R2);
  T0 = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  T0 = T0 - 273.15;
  
  delay (250);

  V1 = analogRead(pin1);
  R2 = R1 * (1023.0 / (float)V1 - 1.0);
  logR2 = log(R2);
  T1 = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  T1 = T1 - 273.15;

  delay (250);

  V2 = analogRead(pin2);
  R2 = R1 * (1023.0 / (float)V2 - 1.0);
  logR2 = log(R2);
  T2 = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  T2 = T2 - 273.15;

  delay (250);

//thermistor3 not connected!
//  V3 = analogRead(pin3);
//  R2 = R1 * (1023.0 / (float)V3 - 1.0);
//  logR2 = log(R2);
//  T3 = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
//  T3 = T3 - 273.15;

//  delay (250);

  AVG = (T0+T1+T2)/3.;

  
//  Serial.print("Time: ");
//  Serial.print(millis()/1000);

//  Serial.print("Temperature: ");
//  Serial.print(T3);
//  Serial.print("|");
  Serial.print(T2);
  Serial.print("|");
  Serial.print(T1);
  Serial.print("|");
  Serial.print(T0);
  Serial.print("|");
  Serial.print(AVG);
  Serial.println("|");
  
  delay(150);
}
