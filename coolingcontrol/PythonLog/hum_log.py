#################################################
#	Humidity logger for cooling system	#
#						#
#	Author: Sigrid Scherl			#
#	Date: June 2021				#
#						#
#################################################

import serial
import time
from datetime import date

# Set up the serial line
hyt_serial = serial.Serial('/dev/ttyACM0', 57600)
time.sleep(2)

# Get time in format hh:mm:ss (for terminal)
def current_time():
	return time.strftime('%H:%M:%S', time.localtime())

# Get time in format hh-mm-ss (for file name)
def current_time2():
	return time.strftime('%H-%M-%S', time.localtime())

# Get date in format dd/mm/yyyy (for terminal)
def current_date():
	return date.today().strftime('%d/%m/%Y')

# Get date in format yyyy-mm-dd (for file name)
def current_date2():
	return date.today().strftime('%Y-%m-%d')

# Print horizontal line
def hline():
	return '------------------------------------------------------------------------'


### Read and record the data ###

# Open log text file
mypath = '/home/labuser/coolingcontrol/Logs'
file = open(mypath+'/Hlog_'+current_date2()+'_'+current_time2()+'.txt','w')

# Print to terminal
print hline(), '\nHumidity log started on', current_date(), 'at', current_time(), '\n', hline()

# Print to file
file.write('Humidity log started on ' + current_date() + ' at ' + current_time() +'\n\n')
file.write('Time \t\t Rel. Humidity (%) \t Temperature (C) \n' + hline() + '\n')

# Get data from Arduino and save it
while 1:
	hyt_line	= hyt_serial.readline()		# read a byte string
	hyt_decoded	= hyt_line.decode()		# decode byte string into Unicode
	hyt_stripped	= hyt_decoded.rstrip()		# remove \n and \r
	hyt_data	= hyt_stripped.split('|')

	if len(hyt_data) == 2 and len(hyt_data[0]) > 3 and len(hyt_data[1]) > 3: # Skip lines that don't contain all measured values
		
		temperature	= float(hyt_data[0])
		humidity	= float(hyt_data[1])

		print 'Time:', current_time(), ' Rel Humidity = %2.2f' % humidity, chr(37), ' Temperature = %2.2f' % temperature, 'C'
		
		file.write(current_time() + '\t %2.2f' % humidity + '\t\t %2.2f' %temperature + '\n')

	time.sleep(0.1)            			# wait (sleep) 0.1 seconds

# End of readout/log
print hline(), '\nLog file saved in ', mypath, '\n', hline()

file.close()
ser_hum.close()
