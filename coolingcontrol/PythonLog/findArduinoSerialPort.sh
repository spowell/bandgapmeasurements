#!/bin/bash

for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
    (
        syspath="${sysdevpath%/dev}"
        devname="$(udevadm info -q name -p $syspath)"
        [[ "$devname" == "bus/"* ]] && exit
        eval "$(udevadm info -q property --export -p $syspath)"
        [[ -z "$ID_SERIAL" ]] && exit
        #echo "/dev/$devname - $ID_SERIAL"
        if [[ $ID_SERIAL == "FTDI_FT232R_USB_UART_AR0JYI3I" ]]; then
            #statements
            echo "/dev/$devname"
        fi
    )
done
