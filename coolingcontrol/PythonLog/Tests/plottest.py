import matplotlib.pyplot as plt

data = [1,2,3,4,5,6,7]


plt.plot(data)
plt.xlabel('Time (seconds)')
plt.ylabel('Potentiometer Reading')
plt.title('Potentiometer Reading vs. Time')
plt.savefig("mygraph.png")
