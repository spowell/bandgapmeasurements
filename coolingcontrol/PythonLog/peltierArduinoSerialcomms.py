from pynput import keyboard
import threading
import serial
import sys
import psutil
import os
#import commands
import subprocess

current_system_pid = os.getpid()
ser = None
x = 0
line = ""

# port = os.system("./listusb.sh")
# port = commands.getoutput("./listusb")
# print("port = ", port)

port = subprocess.check_output(['./findArduinoSerialPort.sh'])
# subprocess.check_call(['/my/file/path/programname.sh', 'arg1', 'arg2', arg3])


print("port = ")#, str(port))
print(port.decode("utf-8").rstrip('\n'))


class SerialReaderThread(threading.Thread):

    def run(self):

        global ser
        global port
        # ser = serial.Serial('COM4', baudrate = 9600, timeout = 5)
        ser = serial.Serial()
        ser.port = port.decode("utf-8").rstrip('\n') #'/dev/ttyUSB1'
        ser.baudrate = 9600
        ser.timeout = 5
        ser.setDTR(False)
        ser.open()

        while True:
            global line
            line = ser.readline().decode('utf-8')
            print(line)


class KeyboardThread(threading.Thread):

    def run(self):

        def on_press(key):

            try:
                if key.char == "q":
                    print("quiting...") 
                    ThisSystem = psutil.Process(current_system_pid)
                    ThisSystem.terminate()

                ser.write(raw_input()) #serial write

            except AttributeError:
                format(key)


        with keyboard.Listener(on_press=on_press) as listener:
            listener.join()

        listener = keyboard.Listener(on_press=on_press)
        listener.start()


def updateSetpoint(number):

    # ser.write(str(number))    #python2
    ser.write(str(number).encode())           #python3

def mainFunction():

    serial_thread = SerialReaderThread()
    keyboard_thread = KeyboardThread()

    serial_thread.start()
    keyboard_thread.start()

    serial_thread.join()
    keyboard_thread.join()

    return line
    

