#########################################################
#	Temperature logger for cooling system		#
#							#
#	Author: Sigrid Scherl				#
#	Date: May 2021					#
#							#
#	Note: This file is used for testing		#
#	the PID to find optimal gain values.		#
#########################################################

import serial
import time
from datetime import date
#import matplotlib.pyplot as plt

# Set up the serial line
ser = serial.Serial('/dev/ttyUSB0', 9600)
time.sleep(2)

# Get time in format hh:mm:ss
def current_time():
	return time.strftime("%H:%M:%S", time.localtime())

# Get time in format hh-mm-ss (for file name)
def current_time2():
	return time.strftime("%H-%M-%S", time.localtime())

# Get date in format dd/mm/yyyy
def current_date():
	return date.today().strftime("%d/%m/%Y")

# Get date in format yyyy-mm-dd (for file name)
def current_date2():
	return date.today().strftime("%Y-%m-%d")

# Print horizontal line
def hline():
	return "------------------------------------------------------------------------"


### Read and record the data ###

# Open log text file
mypath = "/home/labuser/coolingcontrol/Logs"
file = open(mypath+"/Tlog_"+current_date2()+"_"+current_time2()+".txt","w")

# Print to terminal
print hline()
print "Temperature log started on", current_date(), "at", current_time()
print hline()

# Print to file
file.write("Temperature log started on " + current_date() + " at " + current_time() +"\n\n")
file.write("Time \t\t Average T (C) \t T0 (C) \t T1 (C) \t T2 (C) \n" + hline() + "\n")

# Get data from Arduino and save it
for i in range(1800):				# measuring for 30 minutes

	b = ser.readline()			# read a byte string
	string_n = b.decode()			# decode byte string into Unicode 
	string = string_n.rstrip()		# remove \n and \r
	string_list = string.split("|")

	if len(string_list) > 1:		# Skip lines that don't contain all measured values
		
		#arduino_time = float(string_list[0])

		temp0 = float(string_list[3])

		temp1 = float(string_list[2])

		temp2 = float(string_list[1])

		arduino_avg_temp = float(string_list[4])
		arduino_pid_output = float(string_list[5])
		arduino_pid_value = float(string_list[6])
		arduino_pid_p = float(string_list[7])
		arduino_pid_i = float(string_list[8])
		arduino_pid_d = float(string_list[9])

		avg_temp = (temp0 + temp1 + temp2)/3.
		
		print "Time:", current_time(), ", T0 = %2.2f" % temp0, "C, T1 = %2.2f" % temp1, "C, T2 = %2.2f" % temp2, "C, Ardu Avg T = %2.2f" % arduino_avg_temp, "C, Ardu PID Output = ", arduino_pid_output, " Ardu PID Value = ", arduino_pid_value, 'PID_p = ', arduino_pid_p, 'PID_i = ', arduino_pid_i, 'PID_d = ', arduino_pid_d 
		file.write(current_time() + "\t %2.2f" % avg_temp + "\t\t %2.2f" % temp0 + "\t\t %2.2f" % temp1 + "\t\t %2.2f" % temp2 + "\n")

	time.sleep(0.1)            # wait (sleep) 0.1 seconds

# End of readout/log
print hline()
print "Log file saved in ", mypath
print hline()

file.close()
ser.close()
