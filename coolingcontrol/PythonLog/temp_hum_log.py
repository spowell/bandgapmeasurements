#########################################################
#	Temperature and humidity logger			#
#	for cooling system				#
#							#
#	Author: Sigrid Scherl				#
#	Date: May 2021					#
#							#
#########################################################

import serial
import time
from datetime import date

# Set up serial line for Arduino Nano (thermistors)
# nano_serial = serial.Serial('/dev/ttyUSB0', 9600)
nano_serial = serial.Serial('COM4', 9600)
# Set up serial line for Arduino Uno readout (HYT221 sensor)
# uno_serial = serial.Serial('/dev/ttyACM0', 57600)

time.sleep(2)

# Get time in format hh:mm:ss
def current_time():
	return time.strftime('%H:%M:%S', time.localtime())

# Get time in format hh-mm-ss (for file name)
def current_time2():
	return time.strftime('%H-%M-%S', time.localtime())

# Get date in format dd/mm/yyyy
def current_date():
	return date.today().strftime('%d/%m/%Y')

# Get date in format yyyy-mm-dd (for file name)
def current_date2():
	return date.today().strftime('%Y-%m-%d')

# Print horizontal line
def hline():
	return '------------------------------------------------------------------------'


### Read and record the data ###

# Open log text file
mypath = './Logs/'
file = open(mypath+'/THlog_'+current_date2()+'_'+current_time2()+'.txt','w')

# Print to terminal
print hline(), '\nTemperature and humidity log started on', current_date(), 'at', current_time(), '\n', hline()

# Print to file
file.write('Temperature and humidity log started on ' + current_date() + ' at ' + current_time() +'\n\n')
file.write('Time \t\t Average T (C) \t T0 (C) \t T1 (C) \t T2 (C) \t RH (%) \t HYT T (C) \n' + hline() + '\n')

# Get data from Arduinos and save it
while 1:
	nano_line = nano_serial.readline()			# read a byte string
	# uno_line  = uno_serial.readline()			# read a byte string

	# nano_decoded = nano_line.decode()			# decode byte string into Unicode 
	# uno_decoded  = uno_line.decode()			# decode byte string into Unicode 
	
	# nano_stripped = nano_decoded.rstrip()			# remove \n and \r
	# uno_stripped  = uno_decoded.rstrip()			# remove \n and \r

	# nano_data = nano_stripped.split('|')
	# uno_data  = uno_stripped.split('|')

	print(nano_line)
	print("enter new setpoint.. = ")
	setpoint = raw_input();
	nano_serial.write(setpoint);
	
	# if len(nano_data) > 2 and len(uno_data) > 1 and len(uno_data[0]) > 3 and len(uno_data[1]) > 3 and len(nano_data[0]) > 3 and len(nano_data[1]) > 3 and len(nano_data[2]) > 3:		# Skip lines that don't contain all measured values
	# if len(nano_data) > 2 and len(nano_data[0]) > 3 and len(nano_data[1]) > 3 and len(nano_data[2]) > 3:		# Skip lines that don't contain all measured values
		
	# 	# arduino_time = float(string_list_temp[0])

	# 	# Values from the 3 NTC thermistors
	# 	thermistor0 = float(nano_data[2])
	# 	thermistor1 = float(nano_data[1])
	# 	thermistor2 = float(nano_data[0])
	# 	avg_thermistor_temp = (thermistor0 + thermistor1 + thermistor2)/3.

	# 	# Values from HYT221 humidity sensor
	# 	hyt_temperature = float(uno_data[0])
	# 	hyt_humidity    = float(uno_data[1])
		
	# 	print 'Time:', current_time(), ', T0 = %2.2f' % thermistor0, 'C, T1 = %2.2f' % thermistor1, 'C, T2 = %2.2f' % thermistor2, 'C, Avg T = %2.2f' % avg_thermistor_temp, 'C, RH = %2.2f' % hyt_humidity, chr(37), ', HYT T = %2.2f' % hyt_temperature, 'C'
	# 	file.write(current_time() + '\t %2.2f' % avg_thermistor_temp + '\t\t %2.2f' % thermistor0 + '\t\t %2.2f' % thermistor1 + '\t\t %2.2f' % thermistor2 + '\t\t %2.2f' % hyt_humidity + '\t\t %2.2f' % hyt_temperature + '\n')

	# else:
	# 	print 'Error in reading data!'

	time.sleep(0.1)            				# wait (sleep) 0.1 seconds

# End of readout/log
print hline(), '\nLog file saved in ', mypath, '\n', hline()

file.close()
ser_temp.close()
ser_hum.close()
