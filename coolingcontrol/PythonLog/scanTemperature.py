#import threadSerial2
import peltierArduinoSerialcomms
import time
import threading
import pyvisa as visa	#python3
#import visa	#python2
#import matplotlib
#matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
from struct import unpack
from matplotlib import pylab
from datetime import datetime
from datetime import date
import re
import sys
import subprocess

setpoint_min = int(sys.argv[1])
setpoint_max = int(sys.argv[2])

print("setpoint_max = ", setpoint_max)
print("setpoint_min = ", setpoint_min)

# setpoint_min = 20.0
# setpoint_max = 26.0


now = datetime.now()
current_time = now.strftime("%H_%M_%S")
today = date.today()
d1 = today.strftime("%d-%m-%Y-")
basefilename = "./dataFiles/" + d1 + current_time

class myThread(threading.Thread):

	def run(self):

		peltierArduinoSerialcomms.mainFunction()

def checkSetTempAchieved(setpoint, averageTemp):

	match = False
	if abs(setpoint-averageTemp) < 1:
		match = True
		
	return match


def printCSVfile(setTemps,measuredvoltages):

	filename = basefilename+".csv"
	print("file saved... ",filename)
	f = open(filename, "a")

	line = "averageTemp,measuredvoltage\n"  
	f.write(line)
	for x in range(0,len(setTemps)):
		print(x)
		line = str(setTemps[x])+","+str(measuredvoltages[x])+"\n"  
		f.write(line)

	f.close()
	pass	


def readVoltageFromMeasurementKiethley(scope):

	# scope.read_termination = ","		#needed termination for pyvisa (linux) not needed for nivisa windows
	scope.read_termination = ","		#needed termination for pyvisa (linux) not needed for nivisa windows

	scope.write(':SOUR:FUNC CURR')
	scope.write(':SENS:FUNC "VOLT"')
	scope.write(':SENS:VOLT:PROT 3.0')
	scope.write(':OUTP ON')
	# print("function = ", scope.query(':SENS:function?'))
	print("\n\n reading from kiethley...\n\n")
	returnValues = scope.query('READ?')
	try:
		for x in range(100):
			scope.read_bytes(x)
		pass
	except Exception as e:
		pass
	
	# returnValues = scope.write('READ?')
	
	print("voltage = ")
	voltage = float(re.findall("\d+\.\d+", returnValues)[0])
	print(voltage)

	# voltage = returnValues
	return voltage


rm = visa.ResourceManager()
print(rm.list_resources())

port = subprocess.check_output(['./findKiethleyPort.sh']).decode("utf-8").rstrip('\n')
portaddr = ('ASRL%s::INSTR' % port)
print ("portaddr =")
print(portaddr)
scope2 = rm.open_resource(portaddr) # device for kiethley
# scope2 = rm.open_resource('ASRL/dev/ttyUSB1::INSTR')
# scope2 = rm.open_resource('ASRL/dev/ttyS0::INSTR')

# scope2.write_termination = "/"
scope2.read_termination = "/"		#needed termination for pyvisa (linux) not needed for nivisa windows

scope2.encoding =  "latin-1"
# scope2.timeout =  5
# scope2.read('\n') 
# print(scope2.query('*IDN?'))
print("getting kiethley id...")
print(scope2.query('*IDN?'))
# print(scope2.write('*IDN?'))

# for x in range(100):
# 	print(scope2.read_bytes(x))

# print(scope2.ask('*IDN?'))

# for x in range(10):
# 	readVoltageFromMeasurementKiethley(scope2)

# readVoltageFromMeasurementKiethley(scope2)

# print("voltage = ", voltage)
# print("sddskjfsdkjldfsjkldfsjkldfsdfsklj")


tThread = myThread()
tThread.start()
time.sleep(5)	#need delay for thread start before trying to write to serial


temperatureIncrement = 1
settlingTime = 2

setpoint = setpoint_min

setTemps = []
measuredvoltages = []

while setpoint < setpoint_max:
# for setpoint in range(10):
	
	peltierArduinoSerialcomms.updateSetpoint(setpoint)
	time.sleep(2)
	line = peltierArduinoSerialcomms.line
	print("readLine = %s" % line)

	line_decoded = line						# decode not needed for python3
	# line_decoded = line.decode()			# decode byte string into Unicode for python2 
	line_stripped = line_decoded.rstrip()	# remove \n and \r
	line_data = line_stripped.split('|')

	print("line data = ", line_data)
	
	print("lineData1 = ", line_data[3])
	print("lineData2 = ", line_data[4])
	print("lineData3 = ", line_data[5])

	thermistor1 = float(line_data[3])
	thermistor2 = float(line_data[4])
	thermistor3 = float(line_data[5])
	
	avg_thermistor_temp = (thermistor3 + thermistor1 + thermistor2)/3.

	print("temp1 = ", thermistor3)
	print("temp2 = ", thermistor1)
	print("temp3 = ", thermistor2)
	print("avg_thermistor_temp = ", avg_thermistor_temp)

	#if True:
	if checkSetTempAchieved(setpoint, avg_thermistor_temp):

		#read output voltage
		time.sleep(settlingTime)
		voltage = readVoltageFromMeasurementKiethley(scope2)
		scope2.read_termination = "/"		#needed termination for pyvisa (linux) not needed for nivisa windows
		# voltage = 6.66
		temperature = avg_thermistor_temp

		setTemps.append(temperature)
		# measuredvoltages.append(double(voltage))
		measuredvoltages.append(float("{:.5f}".format(voltage)))

		setpoint += temperatureIncrement
		pass

printCSVfile(setTemps,measuredvoltages)
# plotting the points
plt.plot(setTemps, measuredvoltages,marker='o')
# naming the x axis
plt.xlabel('Temperature (*C)')
# naming the y axis
plt.ylabel('Measured output voltage (mV)')
# giving a title to my graph
plt.title('Output Voltage vs Temperature')
# function to show the plot
filename = basefilename+".png"
plt.savefig(filename)
plt.show()


